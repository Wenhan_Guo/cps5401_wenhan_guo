#include <stdio.h>
#include<stdlib.h>
#include <math.h>

int main()
{
	double result;
	int M=0;
	for(int n=1;n<1000000;n=n+1)
	{
	double x=rand()/(double) RAND_MAX*2-1;
	double y=rand()/(double) RAND_MAX*2-1;
	// printf("x=%lf, y=%lf\n", x,y);
		if( (x*x+y*y) <= 1)
		{
			M=M+1;
			}
	}
	int N=1000000;
	result=M/(double)N*4;
	printf("The approximation to the area of the unit disk is %f.\n",result);
	return 0;
}
