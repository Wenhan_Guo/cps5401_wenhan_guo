#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#define N 20000

int main(int argc, char** argv) {
	int i;
	int j;
	double value=0;
	double A ,B;
	double I[N];
	double h=1/(double)N;
	
	for( int i=0;i<N;i++){
		for (int j=0;j<N;j++)
		       	I[i] =  pow(h,2)*(sin(sqrt(9*(j+0.5)*h)))*sinh(1/1+(((i+0.5)*h)-0.5)*(((i+0.5)*h)-0.5)+(((j+0.5)*h)-0.5)*(((j+0.5)*h)-0.5));
		        value+= I[i];
	}
 printf( "The integral value is :%.16f\n",value);
  return 0;
} 
