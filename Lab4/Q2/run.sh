for NPROC in 1 2 4 #8 16 
do
  export OMP_NUM_THREADS=${NPROC};
  echo "p= ${NPROC}:"
  time ./build
done

