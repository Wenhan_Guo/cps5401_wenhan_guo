#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

extern void dposv_(char *UPLO, int *N, int *NRHS, double *A, int *LDA, double *B, int *LDB, int *INFO);

typedef double (*BasisFunc)(double,double,double);

double bf_1(double x, double y, double z)   {return 1.0;}
double bf_x(double x, double y, double z)   {return x;}
double bf_y(double x, double y, double z)   {return y;}
double bf_z(double x, double y, double z)   {return z;}
double bf_xx(double x, double y, double z)  {return x*x;}
double bf_yy(double x, double y, double z)  {return y*y;}
double bf_zz(double x, double y, double z)  {return z*z;}
double bf_xy(double x, double y, double z)  {return x*y;}
double bf_xz(double x, double y, double z)  {return x*z;}
double bf_yz(double x, double y, double z)  {return y*z;}
double bf_xxx(double x, double y, double z) {return x*x*x;}
double bf_yyy(double x, double y, double z) {return y*y*y;}
double bf_zzz(double x, double y, double z) {return z*z*z;}
double bf_xyy(double x, double y, double z) {return x*y*y;}
double bf_xxy(double x, double y, double z) {return x*x*y;}
double bf_xzz(double x, double y, double z) {return x*z*z;}
double bf_xxz(double x, double y, double z) {return x*x*z;}
double bf_yzz(double x, double y, double z) {return y*z*z;}
double bf_yyz(double x, double y, double z) {return y*y*z;}
double bf_xyz(double x, double y, double z) {return x*y*z;}

int main(int argc, char** argv) {

  // Prepare the basis functions
  int nb = 20; // Number of basis functions.
  BasisFunc* bfunc = (BasisFunc*) malloc( nb * sizeof( BasisFunc ) );
  bfunc[0]  = &bf_1;
  bfunc[1]  = &bf_x;
  bfunc[2]  = &bf_y;
  bfunc[3]  = &bf_z;
  bfunc[4]  = &bf_xx;
  bfunc[5]  = &bf_yy;
  bfunc[6]  = &bf_zz;
  bfunc[7]  = &bf_xy;
  bfunc[8]  = &bf_xz;
  bfunc[9]  = &bf_yz;
  bfunc[10] = &bf_xxx;
  bfunc[11] = &bf_yyy;
  bfunc[12] = &bf_zzz;
  bfunc[13] = &bf_xyy;
  bfunc[14] = &bf_xxy;
  bfunc[15] = &bf_xzz;
  bfunc[16] = &bf_xxz;
  bfunc[17] = &bf_yzz;
  bfunc[18] = &bf_yyz;
  bfunc[19] = &bf_xyz;

  // Assemble the matrix and the vector
  double* A = (double*) calloc( nb * nb, sizeof( double ) );
  double* a = (double*) calloc( nb, sizeof( double ) ); // or b
  double* F = (double*) malloc( nb * sizeof( double ) );

  double x, y, z, f;

  FILE* data_file = fopen(argv[1], "r");
  while ( !feof( data_file ) ) {
    fscanf( data_file, "%lf\t%lf\t%lf\t%lf\n", &x, &y, &z, &f );
    for ( int i = 0; i < nb; i++ ) 
      F[i] = (*bfunc[i])(x,y,z);
    // Update A
    for ( int i = 0; i < nb; i++ )
      for ( int j = 0; j < nb; j++ )
        A[i*nb+j] += F[i] * F[j];
    // Update b
    for ( int i = 0; i < nb; i++ )
      a[i] += f * F[i];
  }
  fclose( data_file );

  // Use Lapack to solve for Aa=B
  char UPLO = 'L';
  int  N    = nb;
  int  NRHS = 1;
  int  LDA  = nb;
  int  LDB  = nb;
  int  INFO;
  dposv_(&UPLO, &N, &NRHS, &(A[0]), &LDA, &(a[0]), &LDB, &INFO);

  // Print out coefficients in a
  printf("The result:\n");
  for ( int j = 0; j < nb; j++ )
    printf("\t%f,", a[j]);
  printf("\n");

  // Clean-up
  free( bfunc );

  free( A );
  free( a );
  free( F );

  return 0;}

