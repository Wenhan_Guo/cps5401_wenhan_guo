#ifndef MyComplex_H
#define MyComplex_H

#include <bits/stdc++.h> 
using namespace std;




double** Add(double** a, double** b, int n, int m);
double** Mul(double** a, double** b, int n, int m, int o);
double** inv(double** a, int num);
complex<double>** GetMatrixInverse(complex<double>** src, int n);
void swap(double* a, double* b);  


#endif
