#include "MyComplex.h"
#include <bits/stdc++.h> 
using namespace std;

// matrix a+b, a and b are n*m matrix
double** Add(double** a, double** b, int n, int m) {
	double** res = new double* [n];
	for (int i = 0; i < n; i++) res[i] = new double[m];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			res[i][j] = a[i][j] + b[i][j];
		}
	}
	return res;
}

// matrix a*b, a is a n*m matrix，b is a m*o matrix
double** Mul(double** a, double** b, int n, int m, int o) {
	double** res = new double* [n];
	double temp = 0.0;
	for (int i = 0; i < n; i++) res[i] = new double[o];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < o; j++) {
			temp = 0.0;
			for (int k = 0; k < m; k++) {
				temp += a[i][k] * b[k][j];
			}
			res[i][j] = temp;
		}
	}
	return res;
}

void swap(double* a, double* b);  
// the inverse of a
double** inv(double** a, int num){
	int* is, * js, i, j, k;
	int n = num;
	double temp, fmax;
	double** tp = new double* [num];
	for (int i = 0; i < num; i++) tp[i] = new double[num];
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			tp[i][j] = a[i][j];
		}
	}
	is = new int[n];
	js = new int[n];
	for (k = 0; k < n; k++)
	{
		fmax = 0.0;
		for (i = k; i < n; i++) {
			for (j = k; j < n; j++)
			{
				temp = fabs(tp[i][j]);
				if (temp > fmax)
				{
					fmax = temp;
					is[k] = i; js[k] = j;
				}
			}
		}

		if ((fmax + 1.0) == 1.0)
		{
			delete[] is;
			delete[] js;
			return NULL;
		}
		if ((i = is[k]) != k)
			for (j = 0; j < n; j++)
				swap(&tp[k][j], &tp[i][j]);
		if ((j = js[k]) != k)
			for (i = 0; i < n; i++)
				swap(&tp[i][k], &tp[i][j]);  
		tp[k][k] = 1.0 / tp[k][k];
		for (j = 0; j < n; j++)
			if (j != k)
				tp[k][j] *= tp[k][k];
		for (i = 0; i < n; i++)
			if (i != k)
				for (j = 0; j < n; j++)
					if (j != k)
						tp[i][j] = tp[i][j] - tp[i][k] * tp[k][j];
		for (i = 0; i < n; i++)
			if (i != k)
				tp[i][k] *= -tp[k][k];
	}
	for (k = n - 1; k >= 0; k--)
	{
		if ((j = js[k]) != k)
			for (i = 0; i < n; i++)
				swap(&tp[j][i], &tp[k][i]);
		if ((i = is[k]) != k)
			for (j = 0; j < n; j++)
				swap(&tp[j][i], &tp[j][k]);
	}
	delete[] is;
	delete[] js;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
		}
	}
	return tp;
}
void swap(double* a, double* b)
{
	double c;
	c = *a;
	*a = *b;
	*b = c;
}

complex<double>** GetMatrixInverse(complex<double>** src, int n) {
	double** A = new double* [n];
	double** B = new double* [n];
	for (int i = 0; i < n; i++) {
		A[i] = new double[n];
		B[i] = new double[n];
		for (int j = 0; j < n; j++) {
			A[i][j] = src[i][j].real();
			B[i][j] = src[i][j].imag();
		}
	}
	double** A1 = inv(A, n);
	double** A1B = Mul(A1, B, n, n, n);
	double** BA1B = Mul(B, A1B, n, n, n);
	double** AjBA1B = Add(A, BA1B, n, n);
	double** AjBA1B_1 = inv(AjBA1B, n);
	double** A1B_AjBA1B_1 = Mul(A1B, AjBA1B_1, n, n, n);

	complex<double>** res = new complex<double> * [n];
	for (int i = 0; i < n; i++) {
		res[i] = new complex<double>[n];
		for (int j = 0; j < n; j++) {
			res[i][j].real(AjBA1B_1[i][j]);
			res[i][j].imag(-1.0 * A1B_AjBA1B_1[i][j]);
		}
	}
	return res;
}
