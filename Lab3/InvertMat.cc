#include <iostream>
#include "MyComplex.h"
#include <stdio.h>
#include <string.h>
#include<iomanip>

using namespace std;

int main() {
	FILE *fp, *fsave;
	double x[6];
	double real[3][3], imag[3][3];
	char buff[512];

	fp=fopen("matA.txt","r");
	fsave=fopen("invA.txt","w");
	for(int i=0; i<3; i++){
		fscanf(fp,"%lf %lf %lf %lf %lf %lf",&x[0],&x[1],&x[2],&x[3],&x[4],&x[5]);
		for(int j=0; j<3; j++){
			real[i][j]=x[2*j];
			imag[i][j]=x[2*j+1];
			//printf("%.8f ",real[i][j]);
		}
	}
#if 1
	complex<double>** temp = new complex<double> * [3];
	for (int i = 0; i < 3; i++) {
		temp[i] = new complex<double>[3];
	}
	complex<double> t0(real[0][0], imag[0][0]);
	complex<double> t1(real[0][1], imag[0][1]);
	complex<double> t2(real[0][2], imag[0][2]);

	complex<double> t3(real[1][0], imag[1][0]);
	complex<double> t4(real[1][1], imag[1][1]);
	complex<double> t5(real[1][2], imag[1][2]);

	complex<double> t6(real[2][0], imag[2][0]);
	complex<double> t7(real[2][1], imag[2][1]);
	complex<double> t8(real[2][2], imag[2][2]);

#else
	cout << "Initial matrix: " << endl;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			temp[i][j].real()=real[i][j];
			temp[i][j].imag()=imag[i][j];
			cout << temp[i][j]<<" ";
		}
		cout << endl;
	}
#endif
	cout << "Initial matrix:" << endl;
	temp[0][0]=t0;
	temp[0][1]=t1;
	temp[0][2]=t2;

	temp[1][0]=t3;
	temp[1][1]=t4;
	temp[1][2]=t5;

	temp[2][0]=t6;
	temp[2][1]=t7;
	temp[2][2]=t8;

	cout<<setiosflags(ios::fixed);//after this setting，setprecision is the decimal we want
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			//printf("%.20f ",temp[i][j]);
			cout<<setprecision(8)<< temp[i][j]<<" ";

		}
		cout << endl;
	}
		
	cout << endl <<"Reverse matrix："<<endl;
	complex<double>** res = GetMatrixInverse(temp, 3);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << res[i][j] << " ";
		}
		cout << endl;
	}

	cout << endl <<"Save："<<endl;
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			real[i][j]=res[i][j].real();
			imag[i][j]=res[i][j].imag();
		}
	}
	int len=0;
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){
			
			x[2*i]=real[i][j];
			x[2*i+1]=imag[i][j];
			printf("%.8f ",x[2*i]);
			printf("%.8f ",x[2*i+1]);
			len += snprintf(buff+len, 512, "%.8f %.8f ",x[2*i],x[2*i+1]);
		}
		len += snprintf(buff+len, 512, "\n");
		
		//cout << endl;
	}
	fwrite(buff, len, 1, fsave);
	return 0;
}
     
