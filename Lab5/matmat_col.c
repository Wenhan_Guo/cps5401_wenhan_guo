#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include "mpi.h"

#define DIM 1200

/* This program computes C = A B
 * where A, B, and C are DIM * DIM matrices,
 * and all storages are separeted row-wise
 * on multiple processors
 *
 * To fix the idea, let A_ij = cos(i+j)
 * and B_ij = sin(i-j)
 */

int main(int argc, char** argv) {
  int nproc, myrank;
  MPI_Init( &argc, &argv );//initialize MPI
  MPI_Comm_size( MPI_COMM_WORLD, &nproc );//called by each process once, process number
  MPI_Comm_rank( MPI_COMM_WORLD, &myrank );//same, id

  // Compute the indices of rows for A and C on each processor
  int i_start, i_end;
  i_start = myrank * DIM / nproc;
  i_end = (myrank+1) * DIM / nproc;
  // Compute the indices of columns for B on each processor
  // (they are identical to i_start and i_end, respectively.
  int j_start, j_end;
  j_start = i_start;
  j_end = i_end;

  int nrow = i_end - i_start;
  int ncol = j_end - j_start;

  // Allocate the space
  double* A = (double*) malloc(nrow*DIM*sizeof(double));
  double* B = (double*) malloc(DIM*ncol*sizeof(double));
  double* C = (double*) calloc(nrow*DIM,sizeof(double)); // C is initialized to zero

  int i, j, k;

  // Initialize A and B
  for ( i = i_start; i < i_end; i++ )
    for ( j = 0; j < DIM; j++ )
      A[(i-i_start)*DIM+j] = cos(i+j);
  for ( i = 0; i < DIM; i++ )
    for ( j = j_start; j < j_end; j++ )
      B[i*ncol+(j-j_start)] = sin(i-j);

  // Computing C = C + A * B
  double wtime_loc, wtime_max;
  wtime_loc = MPI_Wtime();

  // ---  START YOUR IMPLEMENTATION BELOW THIS LINE  --- //
double* buff=(double*) malloc(ncol*DIM*sizeof(double));
int* j_range=malloc(2*nproc*sizeof(int));
int j_range_loc[2]={j_start,j_end};
MPI_Gather(&(j_range_loc[0]),2,MPI_INT,&(j_range[0]),2,MPI_INT,0,MPI_COMM_WORLD);
MPI_Bcast(&(j_range[0]),2*nproc,MPI_INT,0,MPI_COMM_WORLD);

for(i=i_start;i<i_end;i++)
	for(j=0;j<DIM;j++)
		for(k=j_start;k<j_end;k++)
			C[(i-i_start)*DIM+j]+=A[(i-i_start)*DIM+k]*B[j*ncol+(k-j_start)];

int offset, rank_to_send, rank_to_recv;
MPI_Status st;
int j_start_else, j_end_else, ncol_else;
for(offset=1;offset<nproc;offset++){
	rank_to_send =(myrank+offset<nproc)?myrank+offset:myrank+offset-nproc;
	rank_to_recv =(myrank-offset>=0)?myrank-offset:myrank-offset+nproc;
	j_start_else=j_range[2*rank_to_recv];
	j_end_else=j_range[2*rank_to_recv+1];
	ncol_else=j_end_else-j_start_else;
if(true){
MPI_Request request;
MPI_Isend(&B[0],ncol*DIM,MPI_DOUBLE,rank_to_send,0,MPI_COMM_WORLD,&request);
MPI_Recv(&(buff[0]),ncol_else*DIM,MPI_DOUBLE,rank_to_recv,0,MPI_COMM_WORLD,&st);
MPI_Wait(&request,MPI_STATUS_IGNORE);}

for(i=i_start;i<i_end;i++)
        for(j=0;j<DIM;j++)
                for(k=j_start_else;k<j_end_else;k++)
                        C[(i-i_start)*DIM+j]+=A[(i-i_start)*DIM+k]*buff[j*ncol+(k-j_start_else)];
}


  // --- FINISH YOUR IMPLEMENTATION BEFORE THIS LINE --- //

  wtime_loc = MPI_Wtime() - wtime_loc;
  MPI_Reduce( &wtime_loc, &wtime_max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD );

  if ( myrank == 0 )
    printf("The wall clock time using %d processors is %f.\n", nproc, wtime_max);

  // Compute the Frobenious norm of C as a brief check on the correctness
  double C_norm_loc = 0.0, C_norm;
  
  for ( i = i_start; i < i_end; i++ )
    for ( j = 0; j < DIM; j++ )
      C_norm_loc += C[(i-i_start)*DIM+j] * C[(i-i_start)*DIM+j];

  MPI_Reduce( &C_norm_loc, &C_norm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
  
  if ( myrank == 0 ) 
    printf("The Frobenious norm of the product matrix C is: %f.\n", sqrt(C_norm));

  free( A );
  free( B );
  free( C );

  MPI_Finalize();

  return 0;
}
