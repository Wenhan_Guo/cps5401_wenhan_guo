for NPROC in 1 2 3 4 7 12 16 20  
do
  export OMP_NUM_THREADS=${NPROC};
  echo "p= ${NPROC}:"
  time ./matmat_row
done

